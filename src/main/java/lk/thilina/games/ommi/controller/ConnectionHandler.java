/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 27, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.io.IOException;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author Thilina Jayamini
 */
@ServerEndpoint("/omi")
public class ConnectionHandler {

    String q;

    @OnOpen
    public void onOpen(Session session) throws IOException, Exception {

        q = session.getQueryString();
        System.out.println("Connecting ... " + q);

        if (q == null || q.trim().isEmpty() || q.equals("null")) {
            session.getBasicRemote().sendText("msgPlayer Name not found");
            session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "Player Name not found"));
        } else {

            GameController.acceptSession(session);
        }

    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        System.out.println("Recieving from :" + session.getId() + " content : " + message);

        if (message.startsWith("`")) {
            System.out.println("got hb from session " + session);
            return;
        }

        GameController.acceptMessage(message, session);

    }

    @OnClose
    public void onClose(Session session, CloseReason cr) {
        System.out.println("Disconnecting ... " + session.getId());

    }

    @OnError
    public void onError(Throwable t) {
        System.out.println("Error in onError:" + t.getMessage());
    }

}
