/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 11, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.util.Set;
import lk.thilina.games.ommi.game.Card;
import lk.thilina.games.ommi.game.MessageFormatProvider;
import lk.thilina.games.ommi.game.Trick;

/**
 *
 * @author Thilina Jayamini
 */
public class WebFormatProvider implements MessageFormatProvider {

    /* Sample message formats
    trkhA,sQ,s10,d7
    crd8hA,sQ,s10,d7,d8,dK,dA,c8
    sco 1,2
    msg<msg>
    *
     */
    private final StringBuilder sb = new StringBuilder();

    @Override
    public String getTrickInformationMsg(Trick trk) {
        sb.delete(0, sb.length());
        sb.append("trk");
        /*int i = 0;
        for (Card c : cards) {
            if (c != null) {
                sb.append(",").append(c.toString());
                i++;
            }
        }
        if (i == 0) {
            return "trk0";
        }
        sb.replace(3, 4, "" + i);*/
        sb.append(trk.getTrickInfo());
        return sb.toString();
    }

    @Override
    public String getPlayerCardShowMsg(Set<Card> myCards) {
        if (myCards.isEmpty()) {
            return "crd0";
        }
        sb.delete(0, sb.length());
        sb.append("crd").append(myCards.size());
        myCards.stream().forEach(
                t -> {
                    sb.append(",");
                    sb.append(t.toString());
                }
        );
        sb.deleteCharAt(4);
        return sb.toString();
    }

    @Override
    public String getTrumpSelectionData() {
        return "trk4c,d,h,s";
    }

    @Override
    public String getNowPlayingMsg(String msg) {
        return "msgNow Playing " + msg;
    }

    @Override
    public String getGameFinishMsg(int gameWinner) {
        return "msgGame Finished !!! Team " + gameWinner + " won the game.";
    }

    @Override
    public String getTrumpSayMsg(String trumpName) {
        return "tmp" + trumpName;
    }

    @Override
    public String getInvalidMoveMsg() {
        return "msgInvalid move !!";
    }

    @Override
    public String getTrickFinishMsg(String player, int teamOneScore, int teamTwoScore) {
        return "msgWinner is :" + player + "<br>score : Team 1 [" + teamOneScore + "] Team 2 [" + teamTwoScore + "]";
    }

    @Override
    public String getHandFinishMsg(int winningTeam, int teamOneScore, int teamTwoScore) {
        return "msgWinning Team is : Team " + winningTeam + "<br>score : Team 1 [" + teamOneScore + "] Team 2 [" + teamTwoScore + "]";
    }

    @Override
    public String getCardSkingMsg() {
        return "msgSelect Card :";
    }

    @Override
    public String getTrumpAskingMsg() {
        return "msgSelect Trump :) ";
    }

    @Override
    public String getMoveRejctMsg() {
        return "msgYou can't play this move!!";
    }

    @Override
    public String getLastTrickInfo(Trick t) {

        return "lti" + t.getTrickInfo();
    }

    @Override
    public String getPlayerNotification() {
        return "ttlSelect Card";
    }

    @Override
    public String getStatusMsg(boolean state) {
        return state ? "sta1" : "sta0";
    }

}
