/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 22, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

/*
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
 */
/**
 *
 * @author Thilina Jayamini <thilina.k@interblocks.com>
 */
public class SecHelper {

    public static void main(String[] args) throws Exception {

        String e = enc("234950525151");

        System.out.println(e);

        System.out.println("");

        String d = dec(e);

        System.out.println(d);

        System.out.println(enc("234950525151"));
        System.out.println(enc("234950525152"));
        System.out.println(enc("234950525153"));
        System.out.println(enc("234950525154"));

    }

    /*
    static byte[] encNew(String data) throws Exception {
        String secret = "a4fceiy9g5hgve83";
        Key key = new SecretKeySpec(secret.getBytes(), "AES");

// Encrypt
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedData = cipher.doFinal(data.getBytes());

        //return encryptedData;
// Decrypt
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decryptedData = cipher.doFinal(encryptedData);
        return decryptedData;
    }*/
    static String enc(String data) {

        String k1 = "asdh1ger9tdb";
        String k2 = "a4s6d21h0gt6";

        byte[] k1b = k1.getBytes();
        byte[] k2b = k2.getBytes();
        byte[] datab = data.getBytes();

        printB(datab);
        datab = wind(datab);

        printB(datab);
        byte[] xOr = xOr(k1b, datab);

        printB(xOr);
        byte[] res = mix(k2b, xOr);
        printB(res);
        return toHex(res);
    }

    static String dec(String data) {

        String k1 = "asdh1ger9tdb";
        String k2 = "a4s6d21h0gt6";

        byte[] k1b = k1.getBytes();
        byte[] datab = toByte(data);

        printB(datab);
        datab = strip(datab);

        printB(datab);
        byte[] xOr = xOr(k1b, datab);

        printB(xOr);
        byte[] res = wind(xOr);

        printB(res);
        return new String(res);
    }

    private static void printB(byte[] b) {
        for (byte c : b) {
            System.out.print(c);
            //System.out.print(" ");
        }
        System.out.println("");

    }

    private static void print(byte[] b) {
        System.out.println(new String(b));
    }

    private static String toHex(byte[] bytes) {

        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

    public static byte[] toByte(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private static byte[] xOr(byte[] a, byte[] b) {

        byte[] c = new byte[a.length];

        for (int i = 0; i < a.length; i++) {
            c[i] = (byte) (a[i] ^ b[i]);
        }

        return c;
    }

    private static byte[] wind(byte[] a) {

        int s = a[a.length - 1] % 10;

        byte s1 = a[1];
        byte s2 = a[s];

        a[1] = s2;
        a[s] = s1;

        return a;

    }

    private static byte[] mix(byte[] a, byte[] b) {

        byte[] c = new byte[a.length * 2];

        for (int i = 0; i < a.length; i++) {
            c[i * 2] = a[i];
            c[(i * 2) + 1] = b[i];
        }

        return c;
    }

    private static byte[] strip(byte[] bytes) {

        byte[] c = new byte[bytes.length / 2];

        for (int i = 0; i < c.length; i++) {
            c[i] = bytes[(i * 2) + 1];

        }

        return c;
    }

}
