/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 14, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import lk.thilina.games.ommi.game.GameStateListener;
import lk.thilina.games.ommi.game.Player;

/**
 *
 * @author Thilina Jayamini
 */
public class OmiListener implements GameStateListener {

    int[] gameScore = new int[2];
    int[] trickScore = new int[2];
    String[] teams = new String[2];

    private final ConcurrentHashMap<Broker, Player> PEERS;

    public OmiListener(ConcurrentHashMap<Broker, Player> p) {
        this.PEERS = p;
    }

    @Override
    public void markEndTrick() {
        System.out.println("Trick end");
        PEERS.entrySet().forEach((e) -> {
            try {
                e.getKey().send("sco" + teams[(e.getValue().getId() + 1) % 2]
                        + "[We]:" + trickScore[(e.getValue().getId() + 1) % 2]
                        + "," + teams[e.getValue().getId() % 2]
                        + "[Them]:" + trickScore[e.getValue().getId() % 2]);
            } catch (IOException ex) {
                System.out.println("in side markEndTrick():" + ex.getMessage());
            }
        });

    }

    @Override
    public void markEndHand() {
        System.out.println("Hand end");
        trickScore[0] = 0;
        trickScore[1] = 0;
        PEERS.entrySet().forEach((e) -> {
            try {

                e.getKey().send("trk4x&nbsp;,x&nbsp;,x&nbsp;,x&nbsp;");
                e.getKey().send("trk0");
                e.getKey().clearHistory();
                e.getKey().send("tolWe:" + gameScore[(e.getValue().getId() + 1) % 2] + ",Them:" + +gameScore[e.getValue().getId() % 2]);
                e.getKey().send("ttlHello," + e.getValue());

            } catch (IOException ex) {
                System.out.println("in side markEndHand():" + ex.getMessage());
            }
        });
    }

    @Override
    public void setTeamOneTrickScore(int score) {
        trickScore[0] = score;
    }

    @Override
    public void setTeamTwoTrickScore(int score) {
        trickScore[1] = score;
    }

    @Override
    public void setTeamOneGameScore(int score) {
        gameScore[0] = score;
    }

    @Override
    public void setTeamTwoGameScore(int score) {
        gameScore[1] = score;
    }

    @Override
    public void setTrumps(String trump) {
        System.out.println("Trumps are :" + trump);
    }

    public void setTeams(String[] teams) {
        this.teams = teams;
    }

}
