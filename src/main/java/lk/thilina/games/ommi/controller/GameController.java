/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 28, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import javax.websocket.CloseReason;
import javax.websocket.Session;
import lk.thilina.games.ommi.game.Game;
import lk.thilina.games.ommi.game.Player;

/**
 *
 * @author Thilina Jayamini
 */
public class GameController implements Runnable {

    private boolean isGameStarted = false;
    private int playerCount = 0;
    private final Game g;
    private final ConcurrentHashMap<String, Broker> PEERS = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Broker, Player> PLAYERS = new ConcurrentHashMap<>();
    private final OmiListener OL = new OmiListener(PLAYERS);
    private final WebFormatProvider WFP = new WebFormatProvider();
    private final BlockingQueue<Player> q = new LinkedBlockingQueue<>(1);
    private final HashMap<Integer, Player> PENDING_PLAYES = new HashMap<>();

    private static final HashMap<String, GameController> GAMES = new HashMap<>();
    
    

    public GameController() {
        g = new Game(OL, WFP);
    }

    static String createGame() {
        String s = String.valueOf(Math.random());

        while (s.length() < 15) {
            s = s + String.valueOf(Math.random());
        }
        s = s.substring(2, 13);

        GameController gc = new GameController();
        Thread t = new Thread(gc);
        t.start();
        GAMES.put(s, gc);
        return s;
    }

    static java.util.List<String> createGamePlayers(String gameId, java.util.List<String> playerNames,String [] teams) {

        java.util.List<String> tokens = new java.util.ArrayList<>();
        Broker b;
        GameController gc = GAMES.get(gameId);
        gc.OL.setTeams(teams);

        for (String name : playerNames) {

            b = new Broker(null);
            Player p = gc.g.addPlayer(b);
            p.setName(name);
            tokens.add(name+","+encrypt(gameId + p.getId()));
            gc.PENDING_PLAYES.put(p.getId(), p);
        }

        return tokens;
    }

    private static String encrypt(String in) {
        return SecHelper.enc(in);
    }

    private static String decrypt(String in) {
        return SecHelper.dec(in);
    }

    static GameController getGameControl(String token) {
        return GAMES.get(token.substring(0, 11));
    }

    static String getUser(String token) {
        return token.substring(11);
    }

    static void acceptSession(Session session) throws IOException, Exception {
        String plainToken = decrypt(session.getQueryString());
        GameController gc = getGameControl(plainToken);

        if (gc == null) {
            return;
        }

        String user = getUser(plainToken);

        if (gc.PEERS.containsKey(user)) {
            Broker b = gc.PEERS.get(user);
            b.setSession(session);
            b.sendHistory();

            gc.PEERS.put(user, b);

        } else {
            if (!gc.isGameStarted) {
                //Broker b = new Broker(session);
                //Player p = gc.g.addPlayer(b);
                Player p = gc.PENDING_PLAYES.get(Integer.parseInt(user));
                Broker b = (Broker) p.getAgent();
                b.setSession(session);
                b.send("ttlHello," + p);
                gc.PLAYERS.put(b, p);

                gc.PEERS.put(user, b);
                gc.q.put(p);

            } else {
                session.getBasicRemote().sendText("msgCannot connect now");
                session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "Game alredy Started"));
            }
        }

    }

    static void acceptMessage(String message, Session session) {

        String plainToken = decrypt(session.getQueryString());
        GameController gc = getGameControl(plainToken);
        String user = getUser(plainToken);

        if (gc.isGameStarted) {
            gc.PEERS.get(user).setMsg(message);
        }
    }

    @Override
    public void run() {
        try {
            while (!isGameStarted) {
                q.take();
                playerCount++;

                if (playerCount == 4) {
                    isGameStarted = true;
                    System.out.println("game started");
                    g.startGame();
                }
            }

        } catch (Exception ex) {
            System.out.println("run Exception " + ex);
        }

    }

}
