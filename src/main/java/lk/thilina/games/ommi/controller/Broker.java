/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 28, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.websocket.Session;
import lk.thilina.games.ommi.game.InformtionExchange;

/**
 *
 * @author Thilina Jayamini
 */
public class Broker implements InformtionExchange {

    private Session s;
    private final BlockingQueue<String> q = new LinkedBlockingQueue<>(1);
    private StringBuilder history = new StringBuilder();

    public Broker(Session s) {
        this.s = s;
    }

    public void setMsg(String msg) {
        System.out.println("recive from : " + s.getQueryString() + " msg : " + msg);
        try {
            q.put(msg);
        } catch (InterruptedException ex) {
            System.out.println("Error in setMsg:" + ex.getMessage());
        }
    }

    public void setSession(Session s) {
        this.s = s;
    }

    public void clearHistory() {
        System.out.println("clearing history");
        history = new StringBuilder();
    }

    public void sendHistory() {
        for (String msg : history.toString().split(";")) {
            try {
                s.getBasicRemote().sendText(msg);
            } catch (IOException ex) {
                System.out.println("inside sendHistory()" + ex.getMessage());
            }
        }
    }

    @Override
    public void send(String msg) throws IOException {
        System.out.println("Sending to : " + s.getQueryString() + " msg : " + msg);
        history.append(msg).append(";");
        s.getBasicRemote().sendText(msg);
    }

    @Override
    public String recive() throws InterruptedException {
        String msg = q.take();
        System.out.println("consume from : " + s.getQueryString() + " msg : " + msg);
        return msg;
    }

}
