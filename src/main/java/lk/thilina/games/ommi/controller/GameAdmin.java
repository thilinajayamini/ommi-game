/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 27, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Thilina Jayamini
 */
@WebServlet(name = "GameAdmin", urlPatterns = {"/ga"})
public class GameAdmin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<String> name = new ArrayList<>();
        String[] teams = new String[2];

        for (int i = 1; i < 5; i++) {
            name.add(request.getParameter("player" + i));
        }

        for (int i = 1; i < 3; i++) {
            teams[i - 1] = request.getParameter("team" + i);
        }

        //name.add("Thilina");
        //name.add("Kasun");
        //name.add("Amal");
        //name.add("Jaliya");
        String gameId = GameController.createGame();
        List<String> createGamePlayers = GameController.createGamePlayers(gameId, name, teams);

        String scheme;
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        String serverPortSrt;
        if (serverPort == 0 || serverPort == 80) {
            serverPortSrt = "";
            scheme = "https";
        } else {
            serverPortSrt = ":" + String.valueOf(serverPort);
            scheme = request.getScheme();
        }

        serverPortSrt += request.getContextPath();

        for (int i = 0; i < createGamePlayers.size();) {

            String[] strArr = createGamePlayers.get(i).split(",");
            i++;
            String url = scheme + "://" + serverName + serverPortSrt + "/omi.html?player=" + strArr[1];

            request.setAttribute("player" + i, strArr[0]);
            request.setAttribute("link" + i, url);
            request.setAttribute("id" + i, strArr[1]);

        }

        request.setAttribute("users", createGamePlayers);
        request.setAttribute("teams", teams);
        javax.servlet.RequestDispatcher dis = request.getRequestDispatcher("teamLinks.jsp");
        dis.forward(request, response);

        /*response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Results</title>");
            out.println("<script src=\"gameAdmin.js\" type=\"text/javascript\"></script>");
            out.println("<link href=\"idx.css\" rel=\"stylesheet\" type=\"text/css\"/>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Results:</h1>");
            out.println("<ol>");
            String scheme;
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();
            String serverPortSrt;
            System.out.println("Request URL -->" + request.getRequestURI());
            if (serverPort == 0 || serverPort == 80) {
                serverPortSrt = "";
                scheme = "https";
            } else {
                serverPortSrt = ":" + String.valueOf(serverPort);
                scheme = request.getScheme();
            }
            //ssl-omi-omi-game.1d35.starter-us-east-1.openshiftapps.com
            createGamePlayers.forEach((createGamePlayer) -> {
                String[] strArr = createGamePlayer.split(",");
                String url = scheme + "://" + serverName + serverPortSrt + "/omi.html?player=" + strArr[1];
                out.println("<li><a id=\"s" + strArr[1] + "\" target=\"_blank\" href=\"" + url + "\">Link for player " + strArr[0] + "</a>");
                out.println("<input  type=\"button\" value=\"Copy to Clipboard\" onClick=\"copythis('s" + strArr[1] + "');\"></li>");
            });
            out.println("</ol>");
            out.println("</body>");
            out.println("</html>");
        }*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        java.io.InputStream manifestStream = getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF");
        java.util.jar.Manifest mf = new java.util.jar.Manifest();
        try {
            mf.read(manifestStream);
        } catch (java.io.IOException e) {
            System.out.println(e.getMessage());
        }
        java.util.jar.Attributes attributes = mf.getMainAttributes();

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(attributes.getValue("Copyright") + " Online Ommi " + attributes.getValue("Built-Date"));
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
