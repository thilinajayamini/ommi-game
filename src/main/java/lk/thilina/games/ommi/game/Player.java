/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Player {

    private final int id;
    private String name;

    private final java.util.SortedSet<Card> myCards;
    private final Game game;
    private int score = 0;
    private final InformtionExchange a;

    private final MessageFormatProvider mfp;

    Player(Game g, int id, InformtionExchange agent, MessageFormatProvider msgFormatProvider) {
        this.myCards = new java.util.TreeSet<>();
        game = g;
        this.id = id;
        this.a = agent;
        this.mfp = msgFormatProvider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void addCards(java.util.Collection<Card> cards) {
        myCards.addAll(cards);
    }

    public InformtionExchange getAgent() {
        return a;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     */
    void incrementScore() {
        score++;
    }

    /**
     *
     * @return
     */
    public int getScore() {
        return score;
    }

    /**
     *
     */
    void clearScore() {
        score = 0;
    }

    /**
     *
     * @param msg
     */
    void sendMsg(String msg) {
        try {
            a.send(msg);
        } catch (Exception ex) {
            System.out.println("Error in Player.sendMsg():" + ex.getMessage());
        }
    }

    /**
     *
     */
    void sayTrumps() {

        Class trumpClass = null;

        while (trumpClass == null) {

            sendMsg(mfp.getTrumpAskingMsg());
            sendMsg(mfp.getStatusMsg(true));
            try {

                trumpClass = Util.getTrumpClass(a.recive());

            } catch (Exception ex) {

                System.out.println("Error in Player.sayTrumps():" + ex.getMessage());
            }
        }

        game.setTrumps(trumpClass);
    }

    /**
     *
     */
    public void showCards() {

        sendMsg(mfp.getPlayerCardShowMsg(this.myCards));
    }

    /*
    public void showTrickScore(){
        

    }
    
    
    public void showGameScore(){
        
    }*/
    /**
     *
     * @throws Exception
     */
    void play() throws Exception {

        boolean isInCorrectPlay = true;
        Card card = null;

        while (isInCorrectPlay) {

            sendMsg(mfp.getCardSkingMsg());
            sendMsg(mfp.getStatusMsg(true));
            card = Util.convertToCard(a.recive());

            if (myCards.contains(card) && game.getNextPlayer().equals(this)) {

                game.setCurrentPlayer(this);

                isInCorrectPlay = !game.play(card);

            } else {
                sendMsg(mfp.getMoveRejctMsg());
            }
        }
        myCards.remove(card);
        sendMsg(mfp.getStatusMsg(false));
    }

    boolean hasThisType(Card c) {
        return myCards.stream().anyMatch(t -> t.type.equals(c.type));
    }

    @Override
    public String toString() {
        return "Player [" + id + "] " + name;
    }

    @Override
    public int hashCode() {

        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        return this.id == other.id;
    }

}
