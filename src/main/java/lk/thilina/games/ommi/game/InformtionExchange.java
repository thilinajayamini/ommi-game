/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public interface InformtionExchange {

    /**
     *
     * @param msg
     * @throws java.lang.Exception
     */
    public void send(String msg)throws Exception;

    /**
     *
     * @return
     * @throws java.lang.Exception
     */
    public String recive()throws Exception;
}
