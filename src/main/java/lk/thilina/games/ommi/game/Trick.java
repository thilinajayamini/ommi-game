/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Trick {

    private int idx = 0;
    private final Card[] cards = new Card[4];
    private final Player[] playes = new Player[4];
    private Card winCard;
    private Player winner;
    private final StringBuilder sb = new StringBuilder();

    void addTurn(Player p, Card c) {
        cards[idx] = c;
        playes[idx] = p;
        idx++;
    }

    public Card getWinCard() {
        return winCard;
    }

    public void setWinCard(Card winCard) {
        this.winCard = winCard;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    int getTurn() {
        return idx;
    }

    Card getCard(int i) {
        return cards[i];
    }

    Card[] getCards() {
        return cards;
    }

    Player getPLayer(int i) {
        return playes[i];
    }

    public String getTrickInfo() {
        sb.delete(0, sb.length());
        sb.append(idx);
        for (int i = 0; i < idx; i++) {
            sb.append(cards[i].toString()).append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "-");
        for (int i = 0; i < idx; i++) {
            sb.append(playes[i].getName()).append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        return sb.toString();
    }

    @Override
    public String toString() {
        return "Trick{" + "idx=" + idx + ", cards=[" + Util.arrayToStr(cards) + "], playes=["
                + Util.arrayToStr(playes) + "], winCard=" + winCard + ", winner=" + winner + '}';
    }

}
