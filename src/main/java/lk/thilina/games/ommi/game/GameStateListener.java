/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 27, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public interface GameStateListener {

    void markEndTrick();

    void markEndHand();

    void setTeamOneTrickScore(int score);

    void setTeamTwoTrickScore(int score);

    void setTeamOneGameScore(int score);

    void setTeamTwoGameScore(int score);
    
    void setTrumps(String trumps);
}
