/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

import java.util.Collections;
import java.util.List;

/**
 *
 * @author Thilina Jayamini
 */
public class Game {

    private int gameRound = -1;
    private final List<Card> pack = new java.util.ArrayList<>();
    private int playerCount = 0;
    private final Player[] players = new Player[4];
    private final List<Hand> hands = new java.util.ArrayList<>();
    private final int[] gameScore = new int[2];
    private final StringBuilder sb = new StringBuilder();

    private Class trump;
    private Hand currentHand;
    private Trick currentTrik;
    private Player currentPlayer, nextPlayer;
    private final GameStateListener agent;
    private final MessageFormatProvider mfp;

    private boolean isHandEnd = false;
    private boolean isTrickEnd = false;

    public Game(GameStateListener listenner, MessageFormatProvider formatProvider) {
        this.agent = listenner;
        this.mfp = formatProvider;
    }

    public void startGame() throws Exception {
        if (playerCount != 4) {
            throw new Exception("Min 4 players requred.");
        }
        run();
    }

    /**
     *
     */
    private void startHand() {

        nextPlayer = players[++gameRound % 4];

        currentHand = new Hand();
        hands.add(currentHand);
        currentTrik = currentHand.getCurrentTrik();
        pack.clear();
        pack.addAll(java.util.Arrays.asList(Heart.values()));
        pack.addAll(java.util.Arrays.asList(Spade.values()));
        pack.addAll(java.util.Arrays.asList(Club.values()));
        pack.addAll(java.util.Arrays.asList(Diamond.values()));
        shuffel();
        sendRound(1);
    }

    /**
     *
     * @param agent
     * @return
     */
    public Player addPlayer(InformtionExchange agent) {

        if (playerCount < 4) {

            players[playerCount++] = new Player(this, playerCount, agent, mfp);

            return players[playerCount - 1];

        }

        return null;
    }

    /**
     *
     * @return
     */
    public Player getNextPlayer() {
        return nextPlayer;
    }

    /**
     *
     */
    public void sendCurrentTrickToPlayers() {

        sendToAll(mfp.getTrickInformationMsg(currentTrik));

    }

    /**
     *
     */
    public void announceCurrentPlayer() {

        sendToAll(mfp.getNowPlayingMsg(nextPlayer.toString()));
    }

    private void run() {
        while (getGameWinner() == 0) {

            startHand();

            printPlayerCards();

            nextPlayer = getNextPlayer();
            showTrumsSelection(nextPlayer);
            nextPlayer.sayTrumps();

            while (!isHandEnd) {

                while (!isTrickEnd) {

                    printPlayerCards();
                    nextPlayer = getNextPlayer();

                    sendCurrentTrickToPlayers();

                    announceCurrentPlayer();

                    try {
                        nextPlayer.play();
                    } catch (Exception ex) {
                        System.out.println("Error in run:" + ex);
                    }

                }

                isTrickEnd = false;
            }
            isHandEnd = false;
        }

        sendToAll(mfp.getGameFinishMsg(getGameWinner()));
    }

    /**
     *
     * @return
     */
    public int getGameWinner() {
        if (gameScore[0] >= 10) {
            return 1;
        } else if (gameScore[1] >= 10) {
            return 2;
        } else {
            return 0;
        }
    }

    /**
     *
     */
    private void shuffel() {

        Collections.shuffle(pack);

    }

    /**
     *
     * @param round
     */
    private void sendRound(int round) {

        if (round > 2) {
            return;
        }

        for (int i = 0; i < 4; i++) {
            players[i].addCards(pack.subList((i * 4) + (round - 1) * 16, ((i + 1) * 4) + ((round - 1) * 16)));
        }

    }

    /**
     *
     * @param c
     */
    void setTrumps(Class c) {
        this.trump = c;
        sendToAll(mfp.getTrumpSayMsg(c.getSimpleName()));

        sendRound(2);
    }

    /**
     *
     * @param card
     */
    boolean play(Card card) {

        if (currentHand.getPossition() < 8) {
            currentTrik = currentHand.getCurrentTrik();

            if (currentTrik.getTurn() < 4) {

                if (isValidMove(card)) {
                    currentTrik.addTurn(currentPlayer, card);
                    nextPlayer = players[currentPlayer.getId() % 4];
                    checkGameState();
                    return true;
                } else {

                    this.currentPlayer.sendMsg(mfp.getInvalidMoveMsg());

                }
            }

        }
        return false;
    }

    /**
     *
     * @param card
     * @return
     */
    private boolean isValidMove(Card card) {

        return currentTrik.getTurn() == 0 || currentTrik.getCard(0).getClass().isInstance(card) || !currentPlayer.hasThisType(currentTrik.getCard(0));

    }

    /**
     *
     * @param p
     */
    void setCurrentPlayer(Player p) {
        currentPlayer = p;
    }

    /**
     *
     */
    private void checkGameState() {
        if (currentTrik.getTurn() == 4) {
            //calculate score
            setWinnerAndCard(0);

            for (int i = 1; i < 4; i++) {
                if (currentTrik.getCard(i).getClass().equals(trump)) {

                    if (currentTrik.getWinCard().getClass().equals(trump)) {
                        compareThenSetWinnerAndCard(i);
                    } else {
                        setWinnerAndCard(i);
                    }

                } else {

                    if (!currentTrik.getWinCard().getClass().equals(trump)) {
                        compareThenSetWinnerAndCard(i);
                    }
                }
            }
            finishTrick();

            if (currentHand.getPossition() == 8) {

                finishHand();

            } else {
                
                sendToAll(mfp.getLastTrickInfo(currentTrik));
            }
        }

    }

    /**
     *
     */
    private void finishTrick() {
        currentHand.startTrick();
        currentTrik.getWinner().incrementScore();
        nextPlayer = currentTrik.getWinner();
        currentHand.scoreBoard[0] = players[0].getScore() + players[2].getScore();
        currentHand.scoreBoard[1] = players[1].getScore() + players[3].getScore();
        sendToAll(mfp.getTrickFinishMsg(nextPlayer.toString(), currentHand.scoreBoard[0], currentHand.scoreBoard[1]));
        isTrickEnd = true;
        agent.setTeamOneTrickScore(currentHand.scoreBoard[0]);
        agent.setTeamTwoTrickScore(currentHand.scoreBoard[1]);
        agent.markEndTrick();
    }

    /**
     *
     */
    private void finishHand() {
        for (Player p : players) {
            p.clearScore();
        }
        int result;

        if (currentHand.getTrumpTeam() == currentHand.getWonTeam()) {
            result = 1;
            System.out.println("TrumpTeam won score is 1");
        } else {
            result = 2;
            System.out.println("OtherTeam won score is 2");
        }

        if (hands.size() > 1 && hands.get(hands.size() - 2).getWonTeam() == 0) {//when previos hand is draw.
            result++;
            System.out.println("hand : " + hands + " ,Previos : " + hands.get(hands.size() - 2));
        }

        if (currentHand.getWonTeam() != 0) {
            System.out.println("Update score :" + gameScore[currentHand.getWonTeam() - 1]);
            gameScore[currentHand.getWonTeam() - 1] += result;
            System.out.println("Result :" + result);
        }
        isHandEnd = true;
        agent.setTeamOneGameScore(gameScore[0]);
        agent.setTeamTwoGameScore(gameScore[1]);
        sendToAll(mfp.getHandFinishMsg(currentHand.getWonTeam(), gameScore[0], gameScore[1]));
        agent.markEndHand();
    }

    /**
     *
     * @param i
     */
    private void compareThenSetWinnerAndCard(int i) {

        if (0 < currentTrik.getWinCard().compare(currentTrik.getCard(i))) {
            setWinnerAndCard(i);
        }

    }

    /**
     *
     * @param i
     */
    private void setWinnerAndCard(int i) {
        currentTrik.setWinCard(currentTrik.getCard(i));
        currentTrik.setWinner(currentTrik.getPLayer(i));
    }

    /**
     *
     * @param msg
     */
    private void sendToAll(String msg) {
        for (Player p : players) {
            try {
                p.sendMsg(msg);
            } catch (Exception ex) {
                System.out.println("Error in sendToAll:" + ex.getMessage());
            }
        }
    }

    /**
     *
     *
     */
    private void printPlayerCards() {

        for (Player p : players) {
            p.showCards();

        }
    }

    private void showTrumsSelection(Player nextPlayer) {
        nextPlayer.sendMsg(mfp.getTrumpSelectionData());
    }

}
