/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 11, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

import java.util.Set;

/**
 *
 * @author Thilina Jayamini
 */
public interface MessageFormatProvider {

    public String getTrickInformationMsg(Trick trk);

    public String getNowPlayingMsg(String msg);

    public String getGameFinishMsg(int gameWinner);

    public String getTrumpSayMsg(String trumpName);

    public String getInvalidMoveMsg();

    public String getTrickFinishMsg(String player, int teamOneScore, int teamTwoScore);

    public String getHandFinishMsg(int winningTeam, int teamOneScore, int teamTwoScore);

    public String getCardSkingMsg();

    public String getTrumpAskingMsg();

    public String getMoveRejctMsg();

    public String getPlayerCardShowMsg(Set<Card> myCards);

    public String getTrumpSelectionData();
    
    public String getLastTrickInfo(Trick t);
    
    public String getPlayerNotification();
    
    public String getStatusMsg(boolean state);
}
