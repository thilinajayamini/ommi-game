/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Club extends Card {

    public Club(CardValue value) {
        super();
        type = "c";
        this.value = value;
    }

    static Club[] values() {
        int i = 0;
        Club[] arr = new Club[8];
        for (CardValue v : CardValue.values()) {
            arr[i++] = new Club(v);
        }

        return arr;
    }
}
