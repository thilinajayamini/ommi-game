/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Util {

    /**
     *
     * @param cardName
     * @return
     * @throws java.lang.Exception
     */
    public static Card convertToCard(String cardName) throws Exception {
        String type = cardName.substring(0, 1);
        System.out.println("convertToCard :cardName->" + cardName + " type ->" + type);

        switch (type) {
            case "h":
                return new Heart(CardValue.get(cardName.substring(1)));

            case "s":
                return new Spade(CardValue.get(cardName.substring(1)));

            case "c":
                return new Club(CardValue.get(cardName.substring(1)));

            case "d":
                return new Diamond(CardValue.get(cardName.substring(1)));

            default:
                return null;
        }
    }

    /**
     *
     * @param s
     * @return
     */
    public static Class getTrumpClass(String s) {
        switch (s) {
            case "h":
                return Heart.class;
            case "c":
                return Club.class;
            case "d":
                return Diamond.class;
            case "s":
                return Spade.class;
            default:
                return null;

        }
    }

    static <T> String arrayToStr(T[] arr) {
        StringBuilder sb = new StringBuilder();
        for (T item : arr) {
            sb.append(item.toString()).append(",");
        }
        return sb.toString();
    }

}
