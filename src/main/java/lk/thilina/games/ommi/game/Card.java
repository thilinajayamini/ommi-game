/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Card implements Comparable<Card> {

    CardValue value;
    String type;

    @Override
    public int compareTo(Card o) {
        if (this.type.equals(o.type)) {
            return this.value.compareTo(o.value);
        } else {
            return this.type.compareTo(o.type);
        }
    }

    int compare(Card card) {
        if (this.type.equals(card.type)) {
            return this.value.compareTo(card.value);
        } else {
            return -1;
        }
    }

    @Override
    public String toString() {
        return this.type + this.value.toString();
    }

    @Override
    public int hashCode() {
        return this.type.hashCode() + this.value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (!this.type.equals(other.type)) {
            return false;
        }
        return this.value == other.value;
    }

}
