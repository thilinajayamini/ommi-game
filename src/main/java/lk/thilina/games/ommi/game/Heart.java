/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Heart extends Card {

    public Heart(CardValue value) {
        super();
        type = "h";
        this.value = value;
    }

    static Heart[] values() {
        int i = 0;
        Heart[] hs = new Heart[8];
        for (CardValue v : CardValue.values()) {
            hs[i++] = new Heart(v);
        }

        return hs;
    }
}
