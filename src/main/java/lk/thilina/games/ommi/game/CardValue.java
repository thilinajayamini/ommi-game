/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public enum CardValue {
    oA("A"), oK("K"), oQ("Q"), oJ("J"), l0("10"), o9("9"), o8("8"), o7("7");

    private final String value;

    private CardValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static CardValue get(String cardValue) throws Exception {
        switch (cardValue) {
            case "A":
                return CardValue.oA;
            case "K":
                return CardValue.oK;
            case "Q":
                return CardValue.oQ;
            case "J":
                return CardValue.oJ;
            case "10":
                return CardValue.l0;
            case "9":
                return CardValue.o9;
            case "8":
                return CardValue.o8;
            case "7":
                return CardValue.o7;
            default:
                throw new Exception("invalid value for Card " + cardValue);

        }
    }

}
