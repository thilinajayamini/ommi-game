/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Spade extends Card {

    public Spade(CardValue value) {
        super();
        type = "s";
        this.value = value;
    }

    static Spade[] values() {
        int i = 0;
        Spade[] arr = new Spade[8];
        for (CardValue v : CardValue.values()) {
            arr[i++] = new Spade(v);
        }

        return arr;
    }
}
