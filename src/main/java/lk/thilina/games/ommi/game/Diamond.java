/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public class Diamond extends Card {

    public Diamond(CardValue value) {
        super();
        type = "d";

        this.value = value;
    }

    static Diamond[] values() {
        int i = 0;
        Diamond[] arr = new Diamond[8];
        for (CardValue v : CardValue.values()) {
            arr[i++] = new Diamond(v);
        }

        return arr;
    }
}
