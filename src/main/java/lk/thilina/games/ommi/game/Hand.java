/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.game;

/**
 *
 * @author Thilina Jayamini
 */
public final class Hand {

    private int idx = -1;
    private final Trick[] t = new Trick[8];
    int[] scoreBoard = new int[2];

    public Hand() {
        startTrick();
    }

    Trick getCurrentTrik() {
        if (idx == -1) {
            return null;
        }

        return t[idx];

    }

    int getPossition() {
        return idx;
    }

    void startTrick() {
        idx++;
        if (idx < 8) {
            t[idx] = new Trick();
        }
    }

    int getTrumpTeam() {
        return (t[0].getPLayer(0).getId() % 2);
    }

    int getWonTeam() {
        if (scoreBoard[0] - scoreBoard[1] > 0) {
            return 1;
        } else if (scoreBoard[0] - scoreBoard[1] < 0) {
            return 2;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Hand{" + "idx=" + idx + ", t=[" + Util.arrayToStr(t) + "], scoreBoard=" + scoreBoard + '}';
    }
    
    

}
