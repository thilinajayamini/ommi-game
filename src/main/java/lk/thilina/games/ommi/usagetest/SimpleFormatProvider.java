/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 11, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.usagetest;

import java.util.Set;
import lk.thilina.games.ommi.game.Card;
import lk.thilina.games.ommi.game.MessageFormatProvider;
import lk.thilina.games.ommi.game.Trick;

/**
 *
 * @author Thilina Jayamini
 */
public class SimpleFormatProvider implements MessageFormatProvider {

    private final StringBuilder sb = new StringBuilder();

    @Override
    public String getTrickInformationMsg(Trick trk) {
        sb.delete(0, sb.length());
        sb.append("\nCurrent Tric is -> ");
        /*for (Card c : cards) {
            sb.append(" | ");
            sb.append(c);
        }
        sb.append(" ");*/
        sb.append(trk.getTrickInfo());
        return sb.toString();

    }

    @Override
    public String getNowPlayingMsg(String msg) {
        return "\nNow Playing " + msg;
    }

    @Override
    public String getGameFinishMsg(int gameWinner) {
        return "Game Finished !!! \nTeam " + gameWinner + " won the game.";
    }

    @Override
    public String getTrumpSayMsg(String trumpName) {
        return "Trumps are " + trumpName + "s !!";
    }

    @Override
    public String getInvalidMoveMsg() {
        return "invalid move";
    }

    @Override
    public String getTrickFinishMsg(String player, int teamOneScore, int teamTwoScore) {
        return "Winner is :" + player + "\nscore : Team 1 [" + teamOneScore + "] Team 2 [" + teamTwoScore + "]";
    }

    @Override
    public String getHandFinishMsg(int winningTeam, int teamOneScore, int teamTwoScore) {
        return "Winning Team is : Team " + winningTeam + "\nscore : Team 1 [" + teamOneScore + "] Team 2 [" + teamTwoScore + "]";
    }

    @Override
    public String getCardSkingMsg() {
        return "\nEnter card name :";
    }

    @Override
    public String getTrumpAskingMsg() {
        return "Enter Trump c:Club d:Diamond h:Heart and s:Spade";
    }

    @Override
    public String getMoveRejctMsg() {
        return "You can't play this move!!";
    }

    @Override
    public String getPlayerCardShowMsg(Set<Card> myCards) {
        sb.delete(0, sb.length());
        myCards.stream().forEach(
                t -> {
                    sb.append(" | ");
                    sb.append(t.toString());
                }
        );
        return sb.toString();

    }

    @Override
    public String getTrumpSelectionData() {
        return "c,d,h,s";
    }

    @Override
    public String getLastTrickInfo(Trick t) {
        return t.toString();
    }

    @Override
    public String getPlayerNotification() {
        return "Select Card";
    }

    @Override
    public String getStatusMsg(boolean state) {
        return state ? "1" : "0";
    }

}
