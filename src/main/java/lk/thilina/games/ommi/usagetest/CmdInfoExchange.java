/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.usagetest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import lk.thilina.games.ommi.game.InformtionExchange;

/**
 *
 * @author Thilina Jayamini
 */
public class CmdInfoExchange implements InformtionExchange {

    private static int instanceId = 1;
    private int myId = 0;

    private final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public CmdInfoExchange() {
        myId = instanceId++;
    }

    @Override
    public void send(String msg) {
        System.out.println("from [" + myId + "] : " + msg);
    }

    @Override
    public String recive() {
        try {
            return br.readLine();
        } catch (IOException ex) {

        }
        return null;
    }

}
