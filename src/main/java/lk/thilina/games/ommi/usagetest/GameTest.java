/*
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Jul 26, 2017
 * ommi
 * Online ommi
 */
package lk.thilina.games.ommi.usagetest;

import java.io.IOException;
import lk.thilina.games.ommi.game.Game;
import lk.thilina.games.ommi.game.GameStateListener;
import lk.thilina.games.ommi.game.Player;

/**
 *
 * @author Thilina Jayamini
 */
public class GameTest implements GameStateListener {

    int oneTricScore;
    int twoTricScore;
    int oneScore;
    int twoScore;

    @Override
    public void markEndTrick() {
        System.out.println("Trick end");
    }

    @Override
    public void markEndHand() {
        System.out.println("Hand end");
    }

    @Override
    public void setTeamOneTrickScore(int score) {
        oneTricScore = score;
    }

    @Override
    public void setTeamTwoTrickScore(int score) {
        twoTricScore = score;
    }

    @Override
    public void setTeamOneGameScore(int score) {
        oneScore = score;
    }

    @Override
    public void setTeamTwoGameScore(int score) {
        twoScore = score;
    }

    @Override
    public void setTrumps(String trump) {
        System.out.println("Trumps are :" + trump);
    }

    /**
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, Exception {

        GameTest gt = new GameTest();
        Game g = new Game(gt, new SimpleFormatProvider());

        Player[] ps = new Player[4];

        ps[0] = g.addPlayer(new CmdInfoExchange());
        ps[1] = g.addPlayer(new CmdInfoExchange());
        ps[2] = g.addPlayer(new CmdInfoExchange());
        ps[3] = g.addPlayer(new CmdInfoExchange());

        ps[0].getScore();

        g.startGame();

        System.out.println("");

    }

}
