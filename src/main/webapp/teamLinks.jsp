<%-- 
    Document   : teamLinks
    Created on : Apr 22, 2018, 10:51:08 AM
    Author     : Thilina Kariyawasam 
--%>

<%@ page isELIgnored="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Player Links</title>
        <script src="gameAdmin.js" type="text/javascript"></script>
        <link href="idx.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">
            function loadVersion() {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        document.getElementById("version").innerHTML = this.responseText;
                    }
                };
                xhttp.open("GET", "ga", true);
                xhttp.send();
            }

        </script>
        
        <style>
            label{
                float: none;
            }
            
        </style>
    </head>
    <body onload="loadVersion();">
        <h1>Player Links</h1>

        <fieldset>
            <legend id="top_head">Teams</legend>
            <fieldset>
                <legend>Team 1</legend>

                <label>Name</label><label>${teams[0]}</label>

                <ul>
                    <li><a id="s${id1}" target="_blank" href="${link1}">Link for player ${player1}</a>                
                        <input  type="button" value="Copy to Clipboard" onClick="copythis('s${id1}');">
                    </li>
                    <li>
                        <a id="s${id3}" target="_blank" href="${link3}">Link for player ${player3}</a>
                        <input  type="button" value="Copy to Clipboard" onClick="copythis('s${id3}');">
                    </li>
                </ul>
            </fieldset>
            <br>
            <br>
            <fieldset>
                <legend>Team 2</legend>

                <label>Name     </label><label>${teams[1]}</label>
                <ul>
                    <li><a id="s${id2}" target="_blank" href="${link2}">Link for player ${player2}</a>                
                        <input  type="button" value="Copy to Clipboard" onClick="copythis('s${id2}');">
                    </li>
                    <li>
                        <a id="s${id4}" target="_blank" href="${link4}">Link for player ${player4}</a>                
                        <input  type="button" value="Copy to Clipboard" onClick="copythis('s${id4}');">
                    </li>
                </ul>
            </fieldset>
            <br>
        </fieldset>
        <p>
            Share this link to other players and click the links to start game.<br>
            once all 4 players connected , game will start automatically.
        </p>
        <p id="version"></p>
    </body>
</html>
