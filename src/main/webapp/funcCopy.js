/* 
 * This is personal application .Creted for fun.
 * Created by thilina jayamini 
 * Aug 30, 2017
 * ommi
 * Online ommi
 */


function copythis(ele_id) {
    var myElement = document.querySelector("#" + ele_id);
    //console.log(myElement.value);

    var textArea = document.createElement("textarea");

    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    textArea.style.width = '2em';
    textArea.style.height = '2em';

    textArea.style.padding = 0;

    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    textArea.style.background = 'transparent';

    textArea.value = myElement.href;

    document.body.appendChild(textArea);

    textArea.select();
    document.execCommand('copy');
    document.body.removeChild(textArea);
}